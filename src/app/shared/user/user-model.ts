export class User{
    constructor(readonly id: number, public firstname: string, public lastname: string, public middlename: string){}
}