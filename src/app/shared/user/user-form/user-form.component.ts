import { Component, Input, EventEmitter, Output } from '@angular/core';
import { User } from '../user-model';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {

    constructor() { }
    @Input() user: User;
    @Output() userChange: EventEmitter<User> = new EventEmitter();

    onButtonClick(): void {
        this.userChange.emit(this.user);
    }
}
