import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UserCardComponent } from './shared/user/user-card/user-card.component';
import { UserListComponent } from './shared/user/user-list/user-list.component';
import { UserListItemComponent } from './shared/user/user-list-item/user-list-item.component';
import { UserFormComponent } from './shared/user/user-form/user-form.component';

@NgModule({
  declarations: [
    AppComponent,
    UserCardComponent,
    UserListComponent,
    UserListItemComponent,
    UserFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
